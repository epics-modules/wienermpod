#
#  Supervision PVs for a Wiener/ISEG/MPOD module
#
#  Macros required:
#     DEV   : base device name
#     HOST  : SNMP host
#     SLOT  : Zero based slot number
#     CHAN  : ISEG channel ID, zero based, zero padded (00, 01, ..., 09, 10, ... )
#     VPREC, CPREC : PRECision of Voltage and Current readings
#
#

record(longin, "${P}${R}#Ch${CHAN}-SupvBehav-R")
{
  field(DESC, "Behavior after failures")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(INP,  "@$(HOST) public %(W)outputSupervisionBehavior.u$(SLOT)$(CHAN) INTEGER: 100 i")
  field(FLNK, "${P}${R}#Ch${CHAN}-BehavLow-R")
}

record(scalcout, "${P}${R}#Ch${CHAN}-BehavLow-R")
{
  field(INPL, "${P}${R}#Ch${CHAN}-SupvBehav-R MS")
  field(CALC, "K:=0;UNTIL(@K:=L&3;L:=L>>2;K:=K+1;K>7)")
  field(FLNK, "${P}${R}#Ch${CHAN}-BehavHigh-R")
}

record(scalcout, "${P}${R}#Ch${CHAN}-BehavHigh-R")
{
  field(INPL, "${P}${R}#Ch${CHAN}-BehavLow-R.L MS")
  field(CALC, "K:=0;UNTIL(@K:=L&3;L:=L>>2;K:=K+1;K>7)")
  field(FLNK, "${P}${R}#Ch${CHAN}-BehavPropagate")
}

record(fanout, "${P}${R}#Ch${CHAN}-BehavPropagate")
{
  field(LNK1, "${P}${R}Ch${CHAN}-BehavMinSenseVolt-R")
  field(LNK2, "${P}${R}Ch${CHAN}-BehavMaxSenseVolt-R")
  field(LNK3, "${P}${R}Ch${CHAN}-BehavMaxTermVolt-R")
  field(LNK4, "${P}${R}Ch${CHAN}-BehavMaxCurrent-R")
  field(FLNK, "${P}${R}#Ch${CHAN}-BehavPropagate2")
}

record(fanout, "${P}${R}#Ch${CHAN}-BehavPropagate2")
{
  field(LNK1, "${P}${R}Ch${CHAN}-BehavMaxTemp-R")
  field(LNK2, "${P}${R}Ch${CHAN}-BehavMaxPower-R")
  field(LNK3, "${P}${R}Ch${CHAN}-BehavInhibit-R")
  field(LNK4, "${P}${R}Ch${CHAN}-BehavTimeout-R")
}


record(mbbi, "${P}${R}Ch${CHAN}-BehavMinSenseVolt-R")
{
  field(DESC, "Reaction to outputFailureMinSenseVolt")
  field(INP,  "${P}${R}#Ch${CHAN}-BehavLow-R.A MS")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
}

record(mbbi, "${P}${R}Ch${CHAN}-BehavMaxSenseVolt-R")
{
  field(DESC, "Reaction to outputFailureMaxSenseVolt")
  field(INP,  "${P}${R}#Ch${CHAN}-BehavLow-R.B MS")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
}

record(mbbi, "${P}${R}Ch${CHAN}-BehavMaxTermVolt-R")
{
  field(DESC, "Action 2 outputFailureMaxTermVolt")
  field(INP,  "${P}${R}#Ch${CHAN}-BehavLow-R.C MS")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
}

record(mbbi, "${P}${R}Ch${CHAN}-BehavMaxCurrent-R")
{
  field(DESC, "Reaction to outputFailureMaxCurrent")
  field(INP,  "${P}${R}#Ch${CHAN}-BehavLow-R.D MS")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
}

record(mbbi, "${P}${R}Ch${CHAN}-BehavMaxTemp-R")
{
  field(DESC, "Reaction to outputFailureMaxTemperature")
  field(INP,  "${P}${R}#Ch${CHAN}-BehavLow-R.E MS")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
}

record(mbbi, "${P}${R}Ch${CHAN}-BehavMaxPower-R")
{
  field(DESC, "Reaction to outputFailureMaxPower")
  field(INP,  "${P}${R}#Ch${CHAN}-BehavLow-R.F MS")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
}

record(mbbi, "${P}${R}Ch${CHAN}-BehavInhibit-R")
{
  field(DESC, "Reaction to outputFailureInhibit")
  field(INP,  "${P}${R}#Ch${CHAN}-BehavLow-R.G MS")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
}

record(mbbi, "${P}${R}Ch${CHAN}-BehavTimeout-R")
{
  field(DESC, "Reaction to outputFailureTimeout")
  field(INP,  "${P}${R}#Ch${CHAN}-BehavLow-R.H MS")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
}


record(mbbo, "${P}${R}Ch${CHAN}-BehavMinSenseVolt-S")
{
  field(DESC, "Reaction to outputFailureMinSenseVolt")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
  field(VAL,  "0")
}

record(mbbo, "${P}${R}Ch${CHAN}-BehavMaxSenseVolt-S")
{
  field(DESC, "Reaction to outputFailureMaxSenseVolt")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
  field(VAL,  "0")
}

record(mbbo, "${P}${R}Ch${CHAN}-BehavMaxTermVolt-S")
{
  field(DESC, "Action 2 outputFailureMaxTermVolt")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
  field(VAL,  "0")
}

record(mbbo, "${P}${R}Ch${CHAN}-BehavMaxCurrent-S")
{
  field(DESC, "Reaction to outputFailureMaxCurrent")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
  field(VAL,  "0")
}

record(mbbo, "${P}${R}Ch${CHAN}-BehavMaxTemp-S")
{
  field(DESC, "Reaction to outputFailureMaxTemperature")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
  field(VAL,  "0")
}

record(mbbo, "${P}${R}Ch${CHAN}-BehavMaxPower-S")
{
  field(DESC, "Reaction to outputFailureMaxPower")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
  field(VAL,  "0")
}

record(mbbo, "${P}${R}Ch${CHAN}-BehavInhibit-S")
{
  field(DESC, "Reaction to outputFailureInhibit")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
  field(VAL,  "0")
}

record(mbbo, "${P}${R}Ch${CHAN}-BehavTimeout-S")
{
  field(DESC, "Reaction to outputFailureTimeout")
  field(ZRVL, "0")
  field(ZRST, "Ignore failure")
  field(ONVL, "1")
  field(ONST, "Ramp down")
  field(TWVL, "2")
  field(TWST, "Ch EmergencyOff")
  field(THVL, "3")
  field(THST, "Mod EmergencyOff")
  field(UNSV, "MAJOR")
  field(VAL,  "0")
}

record(longout, "${P}${R}#Ch${CHAN}-SupvBehav-S")
{
  field(DESC, "Behav after failures")
  field(DTYP, "Snmp")
  field(DRVL, "0")
  field(DRVH, "65535")
  field(OUT,  "@$(HOST) guru %(W)outputSupervisionBehavior.u$(SLOT)$(CHAN) INTEGER: 100 i")
}

record(bo, "${P}${R}Ch${CHAN}-CommitBehavior-S")
{
  field(FLNK, "${P}${R}#Ch${CHAN}-CalcBehav-S")
  field(VAL,  "0")
}

record(scalcout, "${P}${R}#Ch${CHAN}-CalcBehav-S")
{
  field(INPA, "${P}${R}Ch${CHAN}-BehavMinSenseVolt-S MS")
  field(INPB, "${P}${R}Ch${CHAN}-BehavMaxSenseVolt-S MS")
  field(INPC, "${P}${R}Ch${CHAN}-BehavMaxTermVolt-S MS")
  field(INPD, "${P}${R}Ch${CHAN}-BehavMaxCurrent-S MS")
  field(INPE, "${P}${R}Ch${CHAN}-BehavMaxTemp-S MS")
  field(INPF, "${P}${R}Ch${CHAN}-BehavMaxPower-S MS")
  field(INPG, "${P}${R}Ch${CHAN}-BehavInhibit-S MS")
  field(INPH, "${P}${R}Ch${CHAN}-BehavTimeout-S MS")
  field(CALC, "I:=7;K:=0;UNTIL(K:=(K<<2)|@I;I:=I-1;I<0)")
  field(OCAL, "K")
  field(DOPT, "Use OCAL")
  field(OUT,  "${P}${R}#Ch${CHAN}-SupvBehav-S PP")
  field(VAL, "0")
}


record(ai, "${P}${R}Ch${CHAN}-SupvMinSenseVolt-R")
{
  field(DESC, "Minimum allowed sense voltage")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(PREC, "$(VPREC)")
  field(EGU,  "V")
  field(INP,  "@$(HOST) public %(W)outputSupervisionMinSenseVoltage.u$(SLOT)$(CHAN) Float: 100 Fn")
}

record(ao, "${P}${R}Ch${CHAN}-SupvMinSenseVolt-S")
{
  field(DESC, "Minimum allowed sense voltage")
  field(DTYP, "Snmp")
  field(PREC, "$(VPREC)")
  field(EGU,  "V")
  field(OUT,  "@$(HOST) guru %(W)outputSupervisionMinSenseVoltage.u$(SLOT)$(CHAN) Float: 100 Fn")
}

record(ai, "${P}${R}Ch${CHAN}-SupvMaxSenseVolt-R")
{
  field(DESC, "Maximum allowed sense voltage")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(PREC, "$(VPREC)")
  field(EGU,  "V")
  field(INP,  "@$(HOST) public %(W)outputSupervisionMaxSenseVoltage.u$(SLOT)$(CHAN) Float: 100 Fn")
}

record(ao, "${P}${R}Ch${CHAN}-SupvMaxSenseVolt-S")
{
  field(DESC, "Maximum allowed sense voltage")
  field(DTYP, "Snmp")
  field(PREC, "$(VPREC)")
  field(EGU,  "V")
  field(OUT,  "@$(HOST) guru %(W)outputSupervisionMaxSenseVoltage.u$(SLOT)$(CHAN) Float: 100 Fn")
}

record(ai, "${P}${R}Ch${CHAN}-SupvMaxTermVolt-R")
{
  field(DESC, "Maximum allowed terminal voltage")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(PREC, "$(VPREC)")
  field(EGU,  "V")
  field(INP,  "@$(HOST) public %(W)outputSupervisionMaxTerminalVoltage.u$(SLOT)$(CHAN) Float: 100 Fn")
}

record(ao, "${P}${R}Ch${CHAN}-SupvMaxTermVolt-S")
{
  field(DESC, "Maximum allowed terminal voltage")
  field(DTYP, "Snmp")
  field(PREC, "$(VPREC)")
  field(EGU,  "V")
  field(OUT,  "@$(HOST) guru %(W)outputSupervisionMaxTerminalVoltage.u$(SLOT)$(CHAN) Float: 100 Fn")
}

record(ai, "${P}${R}Ch${CHAN}-SupvMaxCurrent-R")
{
  field(DESC, "Maximum allowed current")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(PREC, "$(CPREC)")
  field(EGU,  "A")
  field(INP,  "@$(HOST) public %(W)outputSupervisionMaxCurrent.u$(SLOT)$(CHAN) Float: 100 Fn")
}

record(ao, "${P}${R}Ch${CHAN}-SupvMaxCurrent-S")
{
  field(DESC, "Maximum allowed current")
  field(DTYP, "Snmp")
  field(PREC, "$(CPREC)")
  field(EGU,  "A")
  field(OUT,  "@$(HOST) guru %(W)outputSupervisionMaxCurrent.u$(SLOT)$(CHAN) Float: 100 Fn")
}

#
# Not available
#
#record(longin, "${P}${R}Ch${CHAN}-SupervisionMaxTempR")
#{
#  field(DESC, "Maximum allowed temperature")
#  field(DTYP, "Snmp")
#  field(SCAN, "5 second")
#  field(EGU,  "C")
#  field(INP,  "@$(HOST) public %(W)outputSupervisionMaxTemperature.u$(SLOT)$(CHAN) INTEGER: 100")
#}

#
# Not available
#
#record(longout, "${P}${R}Ch${CHAN}-SupervisionMaxTempS")
#{
#  field(DESC, "Maximum allowed temperature")
#  field(DTYP, "Snmp")
#  field(EGU,  "C")
#  field(OUT,  "@$(HOST) guru %(W)outputSupervisionMaxTemperature.u$(SLOT)$(CHAN) INTEGER: 100 i")
#}

record(ai, "${P}${R}Ch${CHAN}-SupvMaxPower-R")
{
  field(DESC, "Maximum allowed power")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(PREC, "$(CPREC)")
  field(EGU,  "W")
  field(INP,  "@$(HOST) public %(W)outputSupervisionMaxPower.u$(SLOT)$(CHAN) Float: 100 Fn")
}

record(ao, "${P}${R}Ch${CHAN}-SupvMaxPower-S")
{
  field(DESC, "Maximum allowed power")
  field(DTYP, "Snmp")
  field(PREC, "$(CPREC)")
  field(EGU,  "W")
  field(OUT,  "@$(HOST) guru %(W)outputSupervisionMaxPower.u$(SLOT)$(CHAN) Float: 100 Fn")
}

