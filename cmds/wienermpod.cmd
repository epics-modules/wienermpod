# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require asyn, 4.33.0
require snmp, 1.0.0.2
require calc, 3.7.1
require iocStats, 3.1.16
# -----------------------------------------------------------------------------
# EPICS siteApps
# -----------------------------------------------------------------------------
# if you change this library version remember to update it at wienerStartupScriptGenerator.sh too;
require wienermpod, 0.0.2

# -----------------------------------------------------------------------------
# Utgard-Lab
# -----------------------------------------------------------------------------
# @field DEV
# @type STRING
# Base device name
epicsEnvSet(DEV, "LabS-Utgard-WienerMPOD")
# -----------------------------------------------------------------------------
# @field HOST
# @type STRING
# IP address of SNMP HOST
epicsEnvSet(HOST, "10.4.3.33")
epicsEnvSet(LOCATION, "Utgard; $(HOST)")

# -----------------------------------------------------------------------------
# Environment settings
# -----------------------------------------------------------------------------
epicsEnvSet(GROUP,   "5")
epicsEnvSet(MIBDIRS, "/usr/share/snmp/mibs:$(wienermpod_DIR)")
epicsEnvSet(W,       "WIENER-CRATE-MIB::")
epicsEnvSet(VPREC,   9)
epicsEnvSet(CPREC,   9)

# -----------------------------------------------------------------------------
# Loading databases based on dynamically generated startup script
# -----------------------------------------------------------------------------
< wienermpod_installed_modules.cmd

# -----------------------------------------------------------------------------
# Loading databases
# -----------------------------------------------------------------------------
# Statistics
iocshLoad("$(iocStats_DIR)iocStats.iocsh", "IOCNAME=$(DEV)")

# -----------------------------------------------------------------------------
# Starting the IOC
# -----------------------------------------------------------------------------
iocInit
