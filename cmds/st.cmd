# This should be a test startup script
require wienermpod

# Set parameters
epicsEnvSet(P,      "$(P=Utg-VIP:)")
epicsEnvSet(Dis,    "$(Dis=Ctrl)")
epicsEnvSet(INDEX,  "1")
epicsEnvSet(HOST,   "172.30.244.136")

# -----------------------------------------------------------------------------
# Environment settings
# -----------------------------------------------------------------------------
epicsEnvSet(GROUP,    "5")
epicsEnvSet(MIBDIRS,  "+$(wienermpod_DIR)")
epicsEnvSet(W,        "WIENER-CRATE-MIB::")
epicsEnvSet(VPREC,    9)
epicsEnvSet(CPREC,    9)
epicsEnvSet(CMD_FILE, "$(wienermpod_DIR)wienermpod_installed_modules.cmd")

# -----------------------------------------------------------------------------
# Loadnig databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

# Loading Wiener crate databases
epicsEnvSet(Dev,    "PwrC")
epicsEnvSet(Idx,    "100")
epicsEnvSet(R,      "${Dis}-${Dev}-${Idx}:")
dbLoadRecords(wienermpod_crate.template, "P=$(P), R=$(R), HOST=$(HOST)")
dbLoadRecords(wienermpod_modules.db,     "P=$(P), R=$(R), HOST=$(HOST)")
dbLoadRecords(wienermpod_groups.db,      "P=$(P), R=$(R), HOST=$(HOST), GROUP=$(GROUP)")

# -----------------------------------------------------------------------------
# Wiener databases according to current crate configuration,
# wienerStartupScriptGenerator.sh called before should guarantee that
# only present blades should be configured;
# -----------------------------------------------------------------------------
# calling a script which request information from Wiener MPOD crate via SNMP
# to configure exactly the modules plugged
# input params:
#     $1 CONST_IP_WIENER
#     $2 CONST_MIBS_PATH
#     $3 CONST_INDEX_CRATE
#     $4 CONST_CMD_FILE
# -----------------------------------------------------------------------------
system "$(wienermpod_DIR)wienerStartupScriptGenerator.sh $(HOST) $(MIBDIRS) $(INDEX) $(CMD_FILE)"

# -----------------------------------------------------------------------------
# then, calling the EPICS startup command to start the IOC, using previously
# generated database definition
# -----------------------------------------------------------------------------
< $(CMD_FILE)

# -----------------------------------------------------------------------------
# Starting the IOC
# -----------------------------------------------------------------------------
iocInit()
